package code;

public class SimpleWithString {

    public String a = "80";
    private String b = "World";

    public SimpleWithString() {
    }

    public SimpleWithString(String a, String b) {
        this.a = a;
        this.b = b;
    }

    public String getA() {
        return a;
    }

    private void setA(String a) {
        this.a = a;
    }

    public String getB() {
        return b;
    }

    public void setB(String b) {
        this.b = b;
    }

    public void doubleA() {
       this.a = this.a + this.a;
    }
    
    public void doubleB() {
        this.b = this.b + this.b;
     }

    public String toString() {
        return String.format("(a:%s, b:%s)", a, b);
    }

}
