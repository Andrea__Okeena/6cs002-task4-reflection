//Finding an Object's class 
package code;

public class Reflection03 {

    public static void main(String[] args) {
		SimpleWithString sString1 =  new SimpleWithString();
        System.out.println("Class: " + sString1.getClass());
        System.out.println("Class name: " + sString1.getClass().getName());
    }
}
