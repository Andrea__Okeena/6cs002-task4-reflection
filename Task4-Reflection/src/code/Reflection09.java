//Finding all fields and values
package code;
import java.lang.reflect.Method;

public class Reflection09 {
	
	public static void main(String[] args) throws Exception{
		SimpleWithString sString1 =  new SimpleWithString();
        Method[] methods = sString1.getClass().getMethods();
        System.out.printf("There are %s methods\n", methods.length);
        for(Method method: methods) {
            System.out.printf ("Method name-%s type-%s parameters:", method.getName(), method.getReturnType());
            Class[] types = method.getParameterTypes();
            for(Class type: types) {
                System.out.print(type.getName() + " ");
            }
            System.out.println();
        }

    }

}
