//Modifying fields
package code;
import java.lang.reflect.Field;

public class Reflection08 {
	
	public static void main(String[] args) throws Exception{
		SimpleWithString sString1 =  new SimpleWithString();
        Field[] fields = sString1.getClass().getDeclaredFields();
        System.out.printf("There are %s fields\n", fields.length);
        for(Field field: fields) {
            field.setAccessible(true);
            String s =  (String) field.get(sString1);
            s += "String";
            field.set(sString1, s);
            System.out.printf ("Field name-%s type-%s value-%s\n", field.getName(), field.getType(), field.get(sString1));
        }

    }

}
