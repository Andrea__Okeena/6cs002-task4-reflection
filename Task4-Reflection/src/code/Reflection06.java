//Checking Accessibility
package code;
import java.lang.reflect.Field;

public class Reflection06 {
	
	public static void main(String[] args) throws Exception{
    	SimpleWithString sString1 = new SimpleWithString();
        Field[] fields = sString1.getClass().getDeclaredFields();
        System.out.printf("There are %s fields\n", fields.length);
        for(Field field: fields) {
            System.out.printf ("Field name-%s type-%s accessible-%s\n", field.getName(), field.getType(), field.isAccessible());
        }
	}
}