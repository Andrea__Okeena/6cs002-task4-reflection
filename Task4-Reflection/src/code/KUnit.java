//KUnit testing with logging 
package code;

import java.util.LinkedList;
import java.util.List;
import java.util.logging.*;

/*
 * The point of this task, especially this class is to show how one can implement
 * their own unit testing functionality
 * This is our equivalent of JUnit
 *
 * My data class, Shape, is an example of an application class that is to be tested
 * My TestShape is my JUnit test case.
 */

public class KUnit {
    private static final String className = KUnit.class.getName();
    private static Logger logger = Logger.getLogger(className);

    private static List<String> checks; // store checks in a list
    private static int checksMade = 0; // number of checks/tests made
    private static int passedChecks = 0;//how many tests passed
    private static int failedChecks = 0;//how many tests passed

    /*
     *
     *this mimicks the output of a Unit test output
     *it is a utility method to be used by assertions
     */

    private static void addToReport(String txt) {
        if (checks == null) {
            checks = new LinkedList<String>();
        }
        checks.add(String.format("%04d: %s", checksMade++, txt));
    }

    /*
     * similar to the straightforward assertEquals() of the JUnit framework
     * Will be used to check whether Length and Width have equal values
     * If they are equal increase passedChecks by 1
     * If they are not equal, increased failedChecks by 1
     */

    public static void checkEquals(String value1, String value2) {
        if (value1.equals(value2)) {
            addToReport(String.format("Test PASS - The value %s is identical to value %s", value1, value2));
            passedChecks++;

        } else {
            addToReport(String.format("Test FAIL - The value %s is not identical to value %s", value1, value2));
            failedChecks++;
            logger.warning(String.format("Test FAIL - The value %s is not identical to value %s", value1, value2));

        }
    }

    /*
     * this is similar to assertNotEquals() in JUnit framework
     * We check whether Length is not equal to Width
     * If they are not equal, increase passedChecks
     * If they are equal, increased failedChecks
     */
    public static void checkNotEquals(String value1, String value2) {
        if (!value1.equals(value2)) {
            addToReport(String.format("Test PASS - The value %s is not identical to value %s", value1, value2));
            passedChecks++;
        } else {
            addToReport(String.format("Test FAIL - The value %s is identical to value %s", value1, value2));
            failedChecks++;
            logger.warning(String.format("Test FAIL - The value %s is identical to value %s", value1, value2));
        }
    }
    /*
     * we generate messages from the report of the checks
     */
    public static void report() {

        Logger parent = logger.getParent();

        Handler [] parentHandlers = parent.getHandlers();
        for(Handler handler : parentHandlers) {
            System.out.println("removing " + handler);
            parent.removeHandler(handler);
        }

        Handler consoleHandler = new ConsoleHandler();
        consoleHandler.setLevel(Level.WARNING);
        logger.addHandler(consoleHandler);

        logger.info("message");

        System.out.printf("%d checks passed\n", passedChecks);
        System.out.printf("%d checks failed\n", failedChecks);
        System.out.println();

        for (String check : checks) {
            System.out.println(check);
        }
    }

}