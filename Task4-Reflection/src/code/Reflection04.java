//Finding A public field and values
package code;
import java.lang.reflect.Field;

public class Reflection04 {

    public static void main(String[] args) throws Exception{
		SimpleWithString sString1 =  new SimpleWithString();
        Field[] fields = sString1.getClass().getFields();
        System.out.printf("There are %s fields\n", fields.length);
        for(Field field: fields) {
            System.out.printf ("Field name-%s type-%s value-%s\n", field.getName(), field.getType(), field.get(sString1));
        }

    }

}
