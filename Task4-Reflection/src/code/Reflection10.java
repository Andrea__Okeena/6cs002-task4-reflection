//Calling a private method
package code;
import java.lang.reflect.Method;

public class Reflection10 {

	public static void main(String[] args) throws Exception {
		SimpleWithString sString1 =  new SimpleWithString();
        Method method = sString1.getClass().getDeclaredMethod("setA", String.class);
        method.setAccessible(true);
        method.invoke(sString1, "New String value");
        System.out.println(sString1);
    }
}
