//TestSimpleWithString class
package code;
import java.io.IOException;


import java.util.logging.*;

import static code.KUnit.*;

/******************************************************************************
 * This code demonstrates the use of the KUnit testing tool. It produces a
 * report that contains messages generated from the check methods.
 *
 * @author Dr Kevan Buckley, University of Wolverhampton, 2019
 ******************************************************************************/

public class TestSimpleWithString {

    void checkConstructorAndAccess(){
    	SimpleWithString s = new SimpleWithString("Hello", "Hi");
        checkEquals(s.getA(), "Hi");
        checkEquals(s.getB(), "Hi");
        checkNotEquals(s.getA(), "Hello");
        checkNotEquals(s.getB(), "Hello");
    }


    public static void main(String[] args) {
    	TestSimpleWithString test1 = new TestSimpleWithString();
    	test1.checkConstructorAndAccess();
        report();
    }


}
